from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.shortcuts import redirect, render
from django.urls import reverse


def login_user(request):
    template_name = "accounts/login.html"
    form = AuthenticationForm()
    message = ""
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = authenticate(
                username=form.cleaned_data["username"],
                password=form.cleaned_data["password"],
            )
            if user is not None:
                login(request, user)
                return redirect(reverse("teste:home"))
            else:
                message = "Login failed!"
    return render(
        request,
        template_name,
        context={"form": form, "message": message})


@login_required()
def logout_user(request):
    logout(request)
    return redirect(reverse("accounts:login"))
