from django.test import TestCase

from accounts.models import User


class AccountsMixin:

    def make_user(self, email="normal@user.com", password="foo"):
        return User.objects.create_user(email, password)


class AccountsTestBase(TestCase, AccountsMixin):
    def setUp(self) -> None:
        return super().setUp()
