from django.urls import resolve, reverse

from accounts import views

from .test_accounts_base import AccountsTestBase


class AccountsViewtest(AccountsTestBase):
    def test_accounts_login_view_function_is_correct(self):
        view = resolve(reverse("accounts:login"))
        self.assertIs(view.func, views.login_user)

    def test_accounts_login_get_view_return_status_code_200(self):
        response = self.client.get(reverse("accounts:login"))
        self.assertEqual(response.status_code, 200)

    def test_accounts_login_view_render_template_correct(self):
        response = self.client.get(reverse("accounts:login"))
        self.assertTemplateUsed(response, "accounts/login.html")

    def test_accounts_login_return_erro_message_in_invalid_credentials(self):
        email = 'user@mail.com'
        password = 'p@ssW0rds'

        self.make_user(email, password)

        self.form_data = {
            "username": email,
            "password": '12345678'
        }

        url = reverse('accounts:login')

        response = self.client.post(url, data=self.form_data, follow=True)

        message = (
            "Por favor, entre com um Endereço de e-mail  "
            "e senha corretos. Note que ambos os campos "
            "diferenciam maiúsculas e minúsculas."
        )
        content = response.content.decode('utf-8')

        self.assertIn(message, content)

    def test_accounts_login_valid_data_successfully_return_status_code_200(
            self
            ):
        email = 'user@mail.com'
        password = 'p@ssW0rds'

        self.make_user(email, password)

        self.form_data = {
            "username": email,
            "password": password
        }

        url = reverse('accounts:login')

        response = self.client.post(url, data=self.form_data, follow=True)

        self.assertEqual(response.status_code, 200)

    def test_accounts_login_valid_data_successfully_redirect_home_page(self):
        email = 'user@mail.com'
        password = 'p@ssW0rds'

        self.make_user(email, password)

        self.form_data = {
            "username": email,
            "password": password
        }

        url = reverse('accounts:login')

        response = self.client.post(url, data=self.form_data, follow=True)

        self.assertEqual(
            response.redirect_chain,
            [(reverse("teste:home"), 302)]
            )

    def test_accounts_logout_redirect_correct_page(self):
        email = 'user@mail.com'
        password = 'p@ssW0rds'

        self.make_user(email, password)

        self.form_data = {
            "username": email,
            "password": password
        }

        self.client.post(
            reverse('accounts:login'),
            data=self.form_data, follow=True
            )

        response = self.client.get(
            reverse("accounts:logout"),
            follow=True
            )
        self.assertEqual(
            response.redirect_chain[0][0],
            (reverse("accounts:login"))
            )
