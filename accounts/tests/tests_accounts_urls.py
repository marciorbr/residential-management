from django.test import TestCase
from django.urls import reverse


class AccountsURLsTest(TestCase):
    def test_accounts_login_url_is_correct(self):
        url = reverse("accounts:login")
        self.assertEquals(url, "/accounts/login/")
